include dir.mak
include $(MAK_DIR)/sys/sys.mak
include $(MAK_DIR)/cc/cc.mak

ALL_SRC_FILES:=$(wildcard $(SRC_DIR)/*.c)

lpaw_src_files:=$(filter-out $(SRC_DIR)/lchkpaw.c,$(ALL_SRC_FILES))

lpaw_obj_files:=$(lpaw_src_files:%=%.obj)
lpaw_o_files:=$(lpaw_src_files:%=%.o)
liblpaw_so:=liblpaw$(SYS_LIB_SFX)

lpawd_obj_files:=$(lpaw_src_files:%=%d.obj)
lpawd_o_files:=$(lpaw_src_files:%=%d.o)
liblpawd_so:=liblpawd$(SYS_LIB_SFX)

lchkpaw_out:=lchkpaw$(SYS_APP_SFX)
lchkpawd_out:=lchkpawd$(SYS_APP_SFX)

IFLAGS:=$(CC_I) $(INC_DIR) $(CC_I) $(PAW_DIR)/include
LFLAGS:=$(CC_L) $(LIB_DIR) $(CC_L) $(PAW_DIR)/lib
BFLAGS:=$(IFLAGS) $(LFLAGS) -fPIC
CFLAGS:=$(IFLAGS) $(LFLAGS) -fPIC

LPATH_SEP=$(if $(SYS_IS_WINDOWS),;,:)
LPATH_NAME:=$(if $(SYS_IS_WINDOWS),PATH,LD_LIBRARY_PATH)
LPATH:=$($(LPATH_NAME))
LPATH:=.$(if $(LD_PATH),$(LPATH_SEP)$(LPATH),)
LPATHS=$(LPATH_NAME)=$(LPATH)
$(info LPATHS=$(LPATHS))

prep_launch_lnk:=$(SYS_SET) $(LPATH_NAME)=$(LIB_DIR)$(LPATH_SEP)$(LPATH) && 
prep_launch_bin:=cd $(BIN_DIR) && $(SYS_SET) $(LPATHS) &&

# TODO: create a macro for the target debugger option
DBG_FLAGS:=-ggdb $(CC_D) _DEBUG

run: build $(BIN_DIR)/$(liblpaw_so)
	$(prep_launch_bin) ./$(lchkpaw_out)

rebuild: clean build

debug: build_debug $(BIN_DIR)/$(liblpawd_so)
	$(prep_launch_bin) gdb --args ./$(lchkpawd_out)

gede: build_debug
	$(prep_launch_bin) gede -- args ./$(lchkpawd_out)

rebuild_debug: clean build_debug

clean:
	$(SYS_DEL) $(BIN_DIR)/*.exe
	$(SYS_DEL) $(BIN_DIR)/*.dll
	$(SYS_DEL) $(LIB_DIR)/*.dll
	$(SYS_DEL) $(SRC_DIR)/*.obj
	$(SYS_DEL) $(BIN_DIR)/*.out
	$(SYS_DEL) $(BIN_DIR)/*.so
	$(SYS_DEL) $(LIB_DIR)/*.so
	$(SYS_DEL) $(SRC_DIR)/*.o
	cd $(PAW_DIR) && $(MAKE) clean

# Quick

build: $(BIN_DIR)/$(lchkpaw_out)

build_lib: $(LIB_DIR)/$(liblpaw_so)

$(BIN_DIR)/lchkpaw.exe: $(BIN_DIR)/libpaw32.dll $(BIN_DIR)/liblpaw32.dll $(SRC_DIR)/lchkpaw.c
	$(CC) $(BFLAGS) $(CC_o) $@ $(SRC_DIR)/lchkpaw.c $(CC_l)lpaw $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/liblpaw32.dll: $(lpaw_obj_files) $(LIB_DIR)/libpaw32.dll
	$(CC) $(BFLAGS) -shared $(CC_o) $@ $(lpaw_obj_files) $(CC_l)paw $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/libpaw.dll: $(PAW_DIR)/lib/libpaw.dll
	cp $< $@

$(BIN_DIR)/lchkpaw: $(BIN_DIR)/libpaw.so $(BIN_DIR)/liblpaw.so $(SRC_DIR)/lchkpaw.c
	$(CC) $(BFLAGS) $(CC_o) $@ $(SRC_DIR)/lchkpaw.c $(CC_l)lpaw $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/liblpaw.so: $(lpaw_o_files) $(LIB_DIR)/libpaw.so
	$(CC) $(BFLAGS) -shared $(CC_o) $@ $(lpaw_o_files) $(CC_l)paw $(CC_l)lua -Wl,-rpath,$(LIB_DIR)
	
$(LIB_DIR)/libpaw.so: $(PAW_DIR)/lib/libpaw.so
	cp $< $@

$(SRC_DIR)/%.obj: $(SRC_DIR)/%
	$(CC) $(CFLAGS) $(CC_o) $@ $(CC_c) $<
	
$(SRC_DIR)/%.o: $(SRC_DIR)/%
	$(CC) $(CFLAGS) $(CC_o) $@ $(CC_c) $<

# Debug
	
build_debug: $(BIN_DIR)/$(lchkpawd_out)

build_debug_lib: $(LIB_DIR)/$(liblpawd_so)

$(BIN_DIR)/lchkpawd.exe: $(BIN_DIR)/libpawd32.dll $(BIN_DIR)/liblpawd32.dll $(SRC_DIR)/lchkpaw.c
	$(CC) $(DBG_FLAGS) $(BFLAGS) $(CC_o) $@ $(SRC_DIR)/lchkpaw.c $(CC_l)lpawd $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/liblpawd32.dll: $(lpawd_obj_files) $(LIB_DIR)/libpawd32.dll
	$(CC) $(DBG_FLAGS) $(BFLAGS) -shared $(CC_o) $@ $(lpawd_obj_files) $(CC_l)pawd $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/libpawd32.dll: $(PAW_DIR)/lib/libpawd32.dll
	cp $< $@

$(BIN_DIR)/lchkpawd: $(BIN_DIR)/libpawd.so $(BIN_DIR)/liblpawd.so $(SRC_DIR)/lchkpaw.c
	$(CC) $(DBG_FLAGS) $(BFLAGS) $(CC_o) $@ $(SRC_DIR)/lchkpaw.c $(CC_l)lpawd $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/liblpawd.so: $(lpawd_o_files) $(LIB_DIR)/libpawd.so
	$(CC) $(DBG_FLAGS) $(BFLAGS) -shared $(CC_o) $@ $(lpawd_o_files) $(CC_l)pawd $(CC_l)lua -Wl,-rpath,$(LIB_DIR)

$(LIB_DIR)/libpawd.so: $(PAW_DIR)/lib/libpawd.so
	cp $< $@

$(SRC_DIR)/%d.obj: $(SRC_DIR)/%
	$(LD_PATHS) && $(CC) $(DBG_FLAGS) $(CFLAGS) $(CC_o) $@ $(CC_c) $<
	
$(SRC_DIR)/%d.o: $(SRC_DIR)/%
	$(CC) $(DBG_FLAGS) $(CFLAGS) $(CC_o) $@ $(CC_c) $<

# Common

$(BIN_DIR)/%.dll: $(LIB_DIR)/%.dll
	cp $< $@

$(BIN_DIR)/%.so: $(LIB_DIR)/%.so
	cp $< $@

$(PAW_DIR)/lib/%:
	cd $(PAW_DIR) && $(MAKE) $(LIB_DIR)/$*

%.mak:

.PHONY: clean
.PHONY: run rebuild build build_lib
.PHONY: debug gede rebuild_debug build_debug build_debug_lib
