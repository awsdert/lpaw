#include "_lpaw.h"

void *chk_alloc( void *ud, void *ptr, size_t size, size_t want )
{
#if 0
	printf( "%s( %p, %p, $zu, %zu )\n", __func__, ud, ptr, size, want );
#endif
	
	if ( !want )
	{
		if ( ptr )
			free( ptr );
		return NULL;
	}
	
	if ( want <= size )
		return ptr;
	
	if ( ptr )
		return realloc( ptr, want );
	
	return malloc( want );
}

/* Robbed from
 * https://stackoverflow.com/questions/59091462/from-c-how-can-i-print-the-contents-of-the-lua-stack
 */

int lua_dumpstack(lua_State *L)
{
	int top=lua_gettop(L);
	printf("Printing %d stack elements\n", top);
	for (int i=1; i <= top; i++)
	{
		printf("%d\t%s\t", i, luaL_typename(L,i));
		switch (lua_type(L, i))
		{
			case LUA_TNUMBER:
				printf("%g\n",lua_tonumber(L,i));
				break;
			case LUA_TSTRING:
				printf("%s\n",lua_tostring(L,i));
				break;
			case LUA_TBOOLEAN:
				printf("%s\n", (lua_toboolean(L, i) ? "true" : "false"));
				break;
			case LUA_TNIL:
				printf("%s\n", "nil");
				break;
			default:
				printf("%p\n",lua_topointer(L,i));
				break;
		}
	}
	return 0;
}

int lua_panic_cb( lua_State *L ) {
	lua_dumpstack(L);
	luaL_traceback(L,L,NULL,0);
	printf( "Error, lua tracback:\n%s", lua_tostring(L,-1) );
	return 0;
}

void lua_error_cb( lua_State *L, char const *text ) {
	printf( "%s", text);
	lua_panic_cb(L);
}

int main()
{
	lua_State *L = lua_newstate( chk_alloc, "check" );
	
	if ( !L )
		return EXIT_FAILURE;
	
	luaL_openlibs(L);
	
	if ( luaL_dofile(L,"chkpaw.lua") != 0 )
	{
		lua_dumpstack(L);
	}
	
	lua_close(L);;
	
	return EXIT_SUCCESS;
}
