#include "_lpaw.h"

int lpaw_term( lua_State *L )
{
	paw_term();
	return 0;
}

int lpaw_init( lua_State *L )
{
	void *ud = NULL;
	lua_Alloc alloc = lua_getallocf( L, &ud );
	int ret = paw_init( (pawAlloc)alloc );
	lua_pushinteger( L, ret );
	return 1;
}

#define lpaw_pushcfunction( L, NAME, CFUNC )\
	do \
	{ \
		lua_pushliteral( L, NAME ); \
		lua_pushcfunction( L, CFUNC ); \
		lua_settable( L, -3 ); \
	} \
	while (0);

int luaopen_liblpaw( lua_State *L )
{
	lua_newtable(L);
	lpaw_pushcfunction( L, "term", lpaw_term );
	lpaw_pushcfunction( L, "init", lpaw_init );
	lpaw_pushcfunction( L, "load_app", lpaw_load_app );
	return 1;
}
