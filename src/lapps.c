#include "_lpaw.h"
#define LPAW_APP_META "liblpaw_app_metatable"

int lpaw_apps___index( lua_State *L );

int lpaw_list_apps( lua_State *L )
{
	struct paw_apps *apps;
	void *ud = NULL;
	
	(void)lua_getalloc( L, &ud );
	
	paw_list_apps( ud );
	
	if ( apps )
	{
		void **ptr = (void**)lua_newuserdata( L, sizeof(void*) );
		
		if ( ptr )
		{
			*ptr = apps;
			
			lpaw_pushcfunction( L, "__index", lpaw_apps___index );
			
			return 1;
		}
		
		paw_unlist_apps( ud, apps );
	}
	
	return 0;
}

int lpaw_app_get_pid( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		lua_pushinteger( L, paw_get_app_pid(app) );
		return 1;
	}
}

int lpaw_app_get_dir( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		
		lua_pushliteral( L, paw_get_app_dir(app) );
		return 1;
	}
}

int lpaw_app_get_source( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		lua_pushliteral( L, paw_get_app_source(ud, app) );
		return 1;
	}
}

int lpaw_app_untrace( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		
		paw_untrace_app( app );
		return 0;
	}
}

int lpaw_app_trace( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		int perm = luaL_checkint( L, 2 );
		lua_pushinteger( L, paw_trace_app( app, perm ) );
		return 1;
	}
}

int lpaw_app_unhook( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		paw_unhook_app( ud, app );
		return 0;
	}
}

int lpaw_app_hook( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		int perm = luaL_checkint( L, 2 );
		lua_pushinteger( L, paw_hook_app( ud, app, perm ) );
		return 1;
	}
}

int lpaw_app_hook( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		char const *dir = luaL_checkstring( L, 2 );
		lua_pushinteger( L, paw_dump_app_state( ud, app, dir ) );
		return 1;
	}
}

int lpaw_apps___index( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;
	char const *name = luaL_checkstring( L, 2 );

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		
		if ( strcmp( name, "__gc" ) == 0 )
		{
			paw_unlist_apps( ud, *app );
			return 0;
		}
	}
	
	return 0;
}
