#ifndef INC__LPAW_H
#define INC__LPAW_H

#include <paw.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

int lpaw_term( lua_State *L );
int lpaw_init( lua_State *L );

int lpaw_unload_app( lua_State *L );
int lpaw_load_app( lua_State *L );

#endif
