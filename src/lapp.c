#include "_lpaw.h"
#define LPAW_APP_META "liblpaw_app_metatable"

int lpaw_app___index( lua_State *L );

int lpaw_load_app( lua_State *L )
{
	struct paw_app *app;
	void *ud = NULL;
	
	(void)lua_getalloc( L, &ud );
	
	if ( lua_isnumber( L, 1 ) )
	{
		int pid = luaL_checkint(L, 1);
		app = paw_load_app( ud, pid );
	}
	else
	{
		char const * path = luaL_checkstring(L, 1);
		app = paw_boot_app( ud, path );
	}
	
	if ( app )
	{
		void **ptr = (void**)lua_newuserdata( L, sizeof(void*) );
		
		if ( ptr )
		{
			*ptr = app;
			
			lpaw_pushcfunction( L, "__index", lpaw_app___index );
			
			return 1;
		}
		
		paw_unload_app( ud, app );
	}
	
	return 0;
}

int lpaw_app___index( lua_State *L )
{
	void **ptr = (void**)luaL_touserdata( L, 1 ), *ud = NULL;
	char const *name = luaL_checkstring( L, 2 );

	(void)lua_getallocf( L, &ud );

	if ( ptr )
	{
		struct paw_app *app = *ptr;
		
		if ( strcmp( name, "__gc" ) == 0 )
		{
			paw_unload_app( ud, *app );
			return 0;
		}
		
		if ( strcmp( name, "pid" ) == 0 )
		{
			lua_pushinteger( L, paw_get_app_pid(app) );
			return 1;
		}
		
		if ( strcmp( name, "dir" ) == 0 )
		{
			lua_pushliteral( L, paw_get_app_dir(app) );
			return 1;
		}
		
		if ( strcmp( name, "source" ) == 0 )
		{
			lua_pushliteral( L, paw_get_app_source(ud, app) );
			return 1;
		}
		
		if ( strcmp( name, "untrace" ) == 0 )
		{
			paw_untrace_app( app );
			return 0;
		}
		
		if ( strcmp( name, "trace" ) == 0 )
		{
			int perm = luaL_checkint( L, 3 );
			lua_pushinteger( L, paw_trace_app( app, perm ) );
			return 1;
		}
		
		if ( strcmp( name, "unhook" ) == 0 )
		{
			paw_unhook_app( ud, app );
			return 0;
		}
		
		if ( strcmp( name, "hook" ) == 0 )
		{
			int perm = luaL_checkint( L, 3 );
			lua_pushinteger( L, paw_hook_app( ud, app, perm ) );
			return 1;
		}
		
		if ( strcmp( name, "dump_state" ) == 0 )
		{
			char const *dir = luaL_checkstring( L, 3 );
			lua_pushinteger( L, paw_dump_app_state( ud, app, dir ) );
			return 1;
		}
	}
	
	return 0;
}
