TOP_DIR?=.

# Detect if we ourselves are a 3rd party inclusion
_DIR_3RDPARTY:=$(wildcard ../../3rdparty) $(wildcard ../../cloned)
CLONED_DIR?=$(if $(_DIR_3RDPARTY),..,$(TOP_DIR)/cloned)

BIN_DIR?=$(TOP_DIR)/bin
LIB_DIR?=$(TOP_DIR)/lib
OBJ_DIR?=$(TOP_DIR)/obj
INC_DIR?=$(TOP_DIR)/include
SRC_DIR?=$(TOP_DIR)/src

MAK_DIR?=$(CLONED_DIR)/mak
clone_mak:=git -C $(CLONED_DIR) clone https://gitlab.com/awsdert/mak.git
pull_mak:=git -C $(MAK_DIR) pull

PAW_DIR?=$(CLONED_DIR)/paw
clone_paw:=git -C $(CLONED_DIR) clone https://gitlab.com/awsdert/paw.git
pull_paw:=git -C $(MAK_DIR) pull

ALL_DIR:=$(BIN_DIR) $(LIB_DIR) $(SRC_DIR) $(OBJ_DIR) $(INC_DIR) $(CLONED_DIR) $(MAK_DIR) $(PAW_DIR)

directories: $(ALL_DIR:%=%/)

%/:
	mkdir $*

$(CLONED_DIR)/%/:
	$(if $(wildcard $@),$(pull_$*),$(clone_$*))

.PHONY: directories
